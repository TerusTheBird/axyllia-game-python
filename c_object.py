# coding: utf-8
import sys, os, operator, pygame
from pygame.locals import *
sys.dont_write_bytecode = True

import c_pyglue
import c_constants as const

import c_animation

class c_object:
	def __init__(self, root, path, pos=None):
		self.root = root
		self.persistent = False
		self.sprite_world  = None
		self.sprite_battle = None
		self.sprite_menu   = None
		self.name = ''
		self.nickname = self.name
		
		self.stats = [ # temp
		[1.0] # size
		]
		
		self.pos = pos # absolute game position
		self.pos_look = [0,-1]
		self.speed    = [0,0]
		self.can_move = True
		self.disable_warps = False
		self.first_step = False
		
		if self.pos == None:
			self.pos = self.block2pos( [0,0] )
		
		self.lookpos_dict = { 'up':[0,-1], 'down':[0,1], 'left':[-1,0], 'right':[1,0] }
		
		with c_pyglue.c_file(path, "r", 'utf-8') as f:
			exec(f.read()) # load deffinition file
	
	# press events
	def press_up(self):
		if self.is_on_grid():
			self.pos_look = self.lookpos_dict['up']
	def press_down(self):
		if self.is_on_grid():
			self.pos_look = self.lookpos_dict['down']
	def press_left(self):
		if self.is_on_grid():
			self.pos_look = self.lookpos_dict['left']
	def press_right(self):
		if self.is_on_grid():
			self.pos_look = self.lookpos_dict['right']
	def press_start(self):
		0
	def press_select(self):
		0
	def press_b(self):
		0
	def press_a(self):
		0
	
	# hold events
	def hold_up(self):
		if self.is_on_grid():
			self.pos_look = self.lookpos_dict['up']
			self.start_walking()
	def hold_down(self):
		if self.is_on_grid():
			self.pos_look = self.lookpos_dict['down']
			self.start_walking()
	def hold_left(self):
		if self.is_on_grid():
			self.pos_look = self.lookpos_dict['left']
			self.start_walking()
	def hold_right(self):
		if self.is_on_grid():
			self.pos_look = self.lookpos_dict['right']
			self.start_walking()
	def hold_start(self):
		0
	def hold_select(self):
		0
	def hold_b(self):
		0
	def hold_a(self):
		0
	
	def pump(self):
		if self.is_on_grid():
			self.update_sprite()
		
		if self.can_move:
			if (not self.is_on_grid()) or self.first_step:
				c_pyglue.ip_add(self.pos, self.speed)
			self.first_step = False
			if self.is_on_grid():
				self.speed = [0,0]
		
		if self.sprite_world != None:
			self.sprite_world.pump()
		if self.sprite_battle != None:
			self.sprite_battle.pump()
		if self.sprite_menu != None:
			self.sprite_menu.pump()
		
		if self.is_player():
			self.root.map_camera_proxy(  c_pyglue.l_add( self.pos, [-64,-64])  )
	
	def update_sprite(self):
		if self.root.state == 'overworld':
			lx, ly = self.pos_look
			#print(repr(self.pos_look))
			sx, sy = self.speed
			if sx != 0 or sy != 0: # if moving
				if sx == 0 and sy <  0:  self.sprite_world.change('walk_up'   ); return;
				if sx == 0 and sy >  0:  self.sprite_world.change('walk_down' ); return;
				if sx <  0 and sy == 0:  self.sprite_world.change('walk_left' ); return;
				if sx >  0 and sy == 0:  self.sprite_world.change('walk_right'); return;
			else:
				if lx == 0 and ly <  0:  self.sprite_world.change('look_up'   ); return;
				if lx == 0 and ly >  0:  self.sprite_world.change('look_down' ); return;
				if lx <  0 and ly == 0:  self.sprite_world.change('look_left' ); return;
				if lx >  0 and ly == 0:  self.sprite_world.change('look_right'); return;
	
	def start_walking(self): # may run before/after main loop dpndng on main game loop, ideally before
		if self.is_on_grid() and self.pos_look != [0,0] and self.can_move:
			dest_pos = self.looking_at()
			if not self.root.object_by_pos( dest_pos ):
				if not self.root.collision_check( dest_pos, const._WALL):
					self.speed = self.pos_look
					self.first_step = True
					self.disable_warps = False
	
	def is_player(self):
		return self.root.object_current is self
	
	def is_on_grid(self):
		x,y = self.pos
		x &= 15; y &= 15;
		return (x == 0 and y == 0)
	
	def get_sprite_world(self):
		return self.sprite_world
	
	def looking_at(self): # get the position you're looking at
		np = c_pyglue.l_mul( self.pos_look, [16,16])
		return c_pyglue.l_add( np, self.pos)
	
	def pos2block(self, pos): # pixel pos to grid pos
		x,y = pos
		x //= 16; y //= 16;
		return [x,y]
	def block2pos(self, pos): # grid pos to pixel pos
		x,y = pos
		x *= 16; y *= 16;
		return [x,y]
	def pos_subpixel(self): # pixel pos inside grid
		x,y = self.pos
		return [ x&15,y&15 ]

'''
if self.root.current_map.check_warp(np):
lx,ly,lz = self.look
if ly > 0 or lx != 0: # if walking down, left, right
w = self.root.current_map.get_warp_atpos(np)
if w:
if w[3] != 'none': # if there is a fancy transition effect
if not self.warping:
self.trigger_warp(w)
'''



