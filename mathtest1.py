def list_neg(l):
	if type(l) == type([]):
		b = l.copy()
		for i in range(len(b)):
			b[i] = -b[i]
		return b
	else:
		b = list(l)
		for i in range(len(b)):
			b[i] = -b[i]
		return tuple(b)

a = [43,21]
print( list_neg(a) )
print( a )
