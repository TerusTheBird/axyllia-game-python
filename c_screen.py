# coding: utf-8
import sys, os, operator, pygame
from pygame.locals import *
sys.dont_write_bytecode = True

import c_pyglue
import c_constants as const

class c_screen:
	def __init__(self, root):
		self.root = root
		self.windows = {
		u'output':[None,[0,0]],
		u'map':   [None,[0,0]], # world, with camera pos
		u'battle':[None,[0,0]], # 
		u'spefx': [None,[0,0]], # fade and others 
		u'postfx':[None,[0,0]]
		}
		self.draws = []
		
		self.size = (160,144)
		self.size_wnd = list(self.size)
		self.windows['map'][0]    = pygame.Surface(self.size, 0, 24)
		self.windows['output'][0] = pygame.Surface(self.size, 0, 24)
		self.final = pygame.display.set_mode( self.size, pygame.RESIZABLE, 24)
		pygame.display.set_caption(const._wnd_title)
	
	def update(self):
		self.flush_draws()
		#
		s = self.windows['map'] # always drawn at 0,0
		if s[0] != None:
			self.windows['output'][0].blit( s[0], (0,0) )
		#
		s = self.windows['spefx']
		if s[0] != None:
			self.windows['output'][0].blit( s[0], c_pyglue.l_neg(s[1]) )
		#
		
		pygame.transform.scale(self.windows['output'][0], self.size_wnd, self.final)
		pygame.display.update()
	
	def camera_move(self, pos, wnd):
		self.windows[wnd][1] = pos
	def camera_pos(self, wnd):
		return self.windows[wnd][1]
	
	def map_camera_move(self, pos):
		self.camera_move(pos, 'map')
	def map_camera_pos(self):
		return self.windows['map'][1]
	
	def draw(self, surface, pos, layer, wnd):
		pos_cammed = pos
		pos_cammed = c_pyglue.l_sub(pos_cammed, self.windows[wnd][1])
		#                    0    1      2           3
		self.draws.append( [wnd, layer, pos_cammed, surface] )
	
	def flush_draws(self):
		
		self.draws.sort()
		
		for d in self.draws:
			d[3].set_colorkey(const._transparent)
			self.windows[ d[0] ][0].blit(d[3], d[2])
		del self.draws[:]
#


if __name__ == u'__main__': # for testing
	s = c_screen(None)
#
