# coding: utf-8
import sys, os, operator, pygame
from pygame.locals import *
sys.dont_write_bytecode = True

_fps = 60     # obvious
_sfp = 960    # animation fixed point scale
_bfp = 65536  # fixed point scale
_transparent = pygame.Color(255,0,254)

_wnd_title = u'Axyllia game'

# collision bitmasks
_WALL  =            0b1 # is it a wall
_WARP  =           0b10 # is it a warp
_TALK  =          0b100 # is there a sign or something here
_GRASS =         0b1000 # is there wild pokemon here
_SURF  =        0b10000 # can you surf here
_UNK1  =       0b100000 # unused 1
_JUMPD =      0b1000000 # can you jump down here
_JUMPL =     0b10000000 # can you jump left here
_JUMPR =    0b100000000 # can you jump right here
_BUSH  =   0b1000000000 # is this a cuttable bush
_STRUP =  0b10000000000 # stair up
_STRDN = 0b100000000000 # stair down

