# coding: utf-8
import sys, os, operator, pygame
from pygame.locals import *
sys.dont_write_bytecode = True

import c_pyglue
import c_constants as const
import c_animation as anim

class c_map:
	def __init__(self, root, path):
		self.root = root
		self.size = None # not 3D anymore
		self.pos = [0,0] # absolute position of the map
		self.offset = [0,0] # map position offset from top left corner
		self.static    = []
		self.animation = []
		self.collision = None
		self.warps     = []
		self.triggers  = []
		self.signs     = []
		
		with c_pyglue.c_file(path, "r", 'utf-8') as f:
			exec(f.read()) # load room file
	
	# for use with map scripts
	def add_sign(self, pos, path):
		np = c_pyglue.l_mul( pos, [16,16] )
		np = c_pyglue.l_add( np, self.pos )
		np = c_pyglue.l_sub( np, self.offset )
		self.signs.append( (np, path) )
	
	def add_warp(self, pos, path):
		np = c_pyglue.l_mul( pos, [16,16] )
		np = c_pyglue.l_add( np, self.pos )
		np = c_pyglue.l_sub( np, self.offset )
		self.warps.append( (np, path) )
	
	def col_pos(self, pos):
		x,y = pos
		x -= self.pos[0]; y -= self.pos[1];
		x += self.offset[0]; y += self.offset[1];
		x //= 16; y //= 16;
		w,h = self.size
		if x < 0:  x = 0
		if x >= w: x = w-1
		if y < 0:  y = 0
		if y >= h: y = h-1
		return self.collision[y*w+x]
	
	def check_warp(self, apos):
		if check_property(apos, const._WARP):
			for s in self.warps:
				if s[0] == pos:
					return s
		return None
	def check_sign(self, apos):
		if check_property(pos, const._TALK):
			for s in self.signs:
				if s[0] == pos:
					return s
		return None
	
	def check_property(self, pos, mask):
		r = self.col_pos(pos)
		if r & mask:
			return True
		else:
			return False
	
	def pump(self): # pump animated graphics
		for a in self.animation:
			a.pump()
	
	def get_graphics(self):
		return (self.static, self.animation)
#

