# coding: utf-8
import sys, os, operator, pygame
from pygame.locals import *
sys.dont_write_bytecode = True

import c_pyglue
import c_constants as const

import c_screen
import c_animation
import c_map
import c_object

class c_game:
	def __init__(self): #   up    down  left  right start elect b     a
		self.button_codes = [K_e,  K_d,  K_s,  K_f,  K_k,  K_g,  K_h,  K_j]
		self.button_hold  = [False,False,False,False,False,False,False,False]
		self.event = None
		self.done  = False
		self.fps_clock = pygame.time.Clock()
		
		self.screen = c_screen.c_screen(self)
		self.state = 'overworld'
		
		self.objects = []
		self.object_current = None
		self.map_current = None
		
		self.rng = 1
	
	def run(self):		
		sys.stdout.write('\n')
		while not self.done: # technically the actual main loop
			self.update()
	
	def update(self): # the 'real' main loop
		self.event_loop()
		self.cycle_rng()
		#sys.stdout.write('              \r')
		#sys.stdout.write(str(self.rng)+'\r')
		#sys.stdout.flush()
		
		for o in self.objects:
			o.pump()
		
		if self.screen != None:
			self.game_draw()
			self.screen.update()
		self.fps_clock.tick(60)
	
	def event_loop(self):
		self.event = pygame.event.get() # store the event in case it may be needed later?
		for e in self.event:
			if e.type == QUIT:
				self.done = True
			if e.type == KEYDOWN:
				if e.key == self.button_codes[0]: # up
					self.button_hold[0] = True
					if self.object_current != None:
						self.object_current.press_up()
				
				if e.key == self.button_codes[1]: # down
					self.button_hold[1] = True
					if self.object_current != None:
						self.object_current.press_down()
				
				if e.key == self.button_codes[2]: # left
					self.button_hold[2] = True
					if self.object_current != None:
						self.object_current.press_left()
				
				if e.key == self.button_codes[3]: # right
					self.button_hold[3] = True
					if self.object_current != None:
						self.object_current.press_right()
				
				if e.key == self.button_codes[4]: # start
					self.button_hold[4] = True
					if self.object_current != None:
						self.object_current.press_start()
				
				if e.key == self.button_codes[5]: # select
					self.button_hold[5] = True
					if self.object_current != None:
						self.object_current.press_select()
				
				if e.key == self.button_codes[6]: # b
					self.button_hold[6] = True
					if self.object_current != None:
						self.object_current.press_b()
				
				if e.key == self.button_codes[7]: # a
					self.button_hold[7] = True
					if self.object_current != None:
						self.object_current.press_a()
			if e.type == KEYUP:
				if e.key == self.button_codes[0]: # up
					self.button_hold[0] = False
				if e.key == self.button_codes[1]: # down
					self.button_hold[1] = False
				if e.key == self.button_codes[2]: # left
					self.button_hold[2] = False
				if e.key == self.button_codes[3]: # right
					self.button_hold[3] = False
				if e.key == self.button_codes[4]: # start
					self.button_hold[4] = False
				if e.key == self.button_codes[5]: # select
					self.button_hold[5] = False
				if e.key == self.button_codes[6]: # b
					self.button_hold[6] = False
				if e.key == self.button_codes[7]: # a
					self.button_hold[7] = False
		self.obj_button_hold()
	
	def obj_button_hold(self):
		if self.object_current != None:
			if self.button_hold[0]:
				self.object_current.hold_up()
			if self.button_hold[1]:
				self.object_current.hold_down()
			if self.button_hold[2]:
				self.object_current.hold_left()
			if self.button_hold[3]:
				self.object_current.hold_right()
			if self.button_hold[4]:
				self.object_current.hold_start()
			if self.button_hold[5]:
				self.object_current.hold_select()
			if self.button_hold[6]:
				self.object_current.hold_b()
			if self.button_hold[7]:
				self.object_current.hold_a()
	
	def game_draw(self):
		if self.state == 'overworld':
			if self.map_current != None: # if a map is loaded then:
				g = self.map_current.get_graphics() # get all of the graphics layers in the map
				for sets in g: # loop through all of them and draw them
					l = 0
					for anim in sets:
						f = anim.get_surface()
						draw_pos = c_pyglue.l_add( anim.pos, self.map_current.pos )
						draw_pos = c_pyglue.l_sub( draw_pos, self.map_current.offset )
						self.draw_proxy(f, draw_pos, l, 'map')
						anim.pump()
						l += 1
				warps = self.map_current.signs
				for w in warps:
					self.draw_proxy( self.object_current.get_sprite_world().get_surface(), w[0], 8, 'map' )
			for o in self.objects:
				s = o.get_sprite_world().get_animation()
				spr_pos = c_pyglue.l_add( o.pos, s.pos )
				self.draw_proxy( s.get_surface(), spr_pos, 0, 'map')
				s.pump()
	
	def map_camera_proxy(self, pos):
		if self.screen != None:
			self.screen.map_camera_move(pos)
	
	def draw_proxy(self, surface, pos, layer, wnd):
		if self.screen != None:
			self.screen.draw(surface, pos, layer, wnd)
	
	def map_load(self, path):
		self.map_current = c_map.c_map(self, path)
	
	def collision_check(self, pos, proprty):
		if self.map_current == None:
			return 0
		return self.map_current.check_property(pos, proprty)
	
	def warp_check(self, pos):
		if self.map_current == None:
			return None
		return self.map_current.check_warp(pos)
	
	def sign_check(self, pos):
		if self.map_current == None:
			return None
		return slef.map_current.check_sign(pos)
	
	def object_by_name(self, name): # we need an object format
		0
	
	def object_by_pos(self, pos):
		for o in self.objects:
			if o.pos == pos:
				return o
		return None
	
	def cycle_rng(self):
		# get a "random" number with RANDU
		self.rng = (self.rng * 65539) & 0x7FFFFFFF
	def get_random_number(self):
		a = self.rng
		self.cycle_rng()
		return a

