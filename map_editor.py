import sys, os, pygame
from pygame.locals import *

class everything:
	def __init__(self, path, c_str=None):
		self.ref_img = pygame.image.load(path,'r')
		self.window_surf = pygame.display.set_mode((1024,700),0,24)
		self.window_surf.fill(pygame.Color(0,0,0))
		self.sx = self.ref_img.get_width() // 16
		self.sy = self.ref_img.get_height() // 16
		
		self.map_x = 0
		self.map_y = 0
		self.mouse_dx = 0
		self.mouse_dy = 0
		self.map_surf = pygame.Surface((self.sx*16,self.sy*16),0,24)
		
		self.menu_surf = pygame.Surface( (1024,33), 0, 24 )
		self.menu_surf.fill(pygame.Color(128,128,128))
		
		self.cl = []
		self.mode = 0
		self.rect = pygame.Rect((0,0),(16,16))
		
		if c_str != None and c_str != '':
			for a in c_str.split(','):
				self.cl.append(eval(a))
		else:
			for w in range(self.sx * self.sy):
				self.cl.append(0)
		
		self.done = False
		while not self.done:
			for self.event in pygame.event.get():
				if self.event.type == QUIT:
					self.done = True
				if self.event.type == KEYDOWN:
					if self.event.key == K_RETURN:
						self.gen_collision_string()
					if self.event.key == K_0: self.mode = 0
					if self.event.key == K_1: self.mode = 1
					if self.event.key == K_2: self.mode = 2
					if self.event.key == K_3: self.mode = 3
					if self.event.key == K_4: self.mode = 4
					if self.event.key == K_5: self.mode = 5
					if self.event.key == K_6: self.mode = 6
					if self.event.key == K_7: self.mode = 7
					if self.event.key == K_8: self.mode = 8
					if self.event.key == K_9: self.mode = 9
				if self.event.type == MOUSEMOTION:
					if self.event.buttons[1]:
						self.rect.x += self.event.rel[0]
						self.rect.y += self.event.rel[1]
					elif self.event.buttons[0]:
						0
			mouse_x, mouse_y = pygame.mouse.get_pos()
			mb1,mb2,mb3 = pygame.mouse.get_pressed()
			if mb1 or mb3:
				rx = mouse_x - self.rect.x
				ry = mouse_y - self.rect.y
				rx //= 16
				ry //= 16
				if mb1:
					self.put_cl(rx, ry, 1)
				if mb3:
					self.put_cl(rx, ry, 0)
			
			self.window_surf.fill(pygame.Color(0,0,0))
			self.draw_map()
			self.window_surf.blit(self.map_surf, self.rect)
			
			#self.window_surf.blit(self.menu_surf, (0,512))
			
			pygame.display.update()
	
	def draw_map(self):
		self.map_surf.blit(self.ref_img,(0,0))
		dsx = self.sx * 16
		dsy = self.sy * 16
		line_color = pygame.Color(100,100,100)
		
		for y in range(self.sy):
			for x in range(self.sx):
				if self.get_cl(x, y):
					r = pygame.Rect((x*16,y*16),(16,16))
					self.map_surf.fill(pygame.Color(255,255,255), r)
		
		for y in range(self.sy):
			pygame.draw.line(self.map_surf, line_color, (0,y*16), (dsx-1,y*16))
		for x in range(self.sx):
			pygame.draw.line(self.map_surf, line_color, (x*16,0), (x*16,dsy-1))
	
	def put_cl(self, x, y, state):
		if x >= 0 and x < self.sx and y >= 0 and y < self.sy:
			c = self.cl[(y*self.sx)+x]
			bm = 1<<self.mode
			c &= ~bm
			c |= (state&1) << self.mode
			self.cl[(y*self.sx)+x] = c
	
	def get_cl(self, x, y):
		if x >= 0 and x < self.sx and y >= 0 and y < self.sy:
			return (self.cl[(y*self.sx)+x] >> self.mode) & 1
		else:
			return 0
	
	def gen_collision_string(self):
		out_str = ''
		out_str += str(self.sx)+','+str(self.sy)+'\n'
		for y in range(self.sy):
			for x in range(self.sx):
				p = y*self.sx+x
				out_str += ("%u" % self.cl[p])
				if (x != self.sx-1) or (y != self.sy-1):
					out_str += ','
			out_str += '\n'
		with open('map_editor_output.txt','w') as f:
			f.write(out_str)
		print('Wrote map')




if __name__ == '__main__':
	pygame.init()
	
	in_str = ''
	
	everything('res/maps/r16/l1.png', in_str)
	
	pygame.quit()
	sys.exit(0)

