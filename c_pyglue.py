# coding: utf-8
import sys, os, operator
sys.dont_write_bytecode = True

_IS_3 = sys.version_info[0] == 3

def c_str(s=None):
	if _IS_3:
		return str(s)
	else:
		return unicode(s)

def longint(n):
	if _IS_3:
		return int(n)
	else:
		return long(n)

def ip_neg(l): # in-place
	for i in range(len(l)):
		l[i] = -l[i]
def l_neg(l): # makes a copy
	b = list(l)
	for i in range(len(b)):
		b[i] = -b[i]
	return b

def ip_mul(l, lm):
	for i in range(len(l)):
		l[i] *= lm[i]
def l_mul(l, lm):
	b = list(l)
	for i in range(len(b)):
		b[i] *= lm[i]
	return b

def ip_add(l, lm):
	for i in range(len(l)):
		l[i] += lm[i]
def l_add(l, lm):
	b = list(l)
	for i in range(len(b)):
		b[i] += lm[i]
	return b

def ip_sub(l, lm):
	for i in range(len(l)):
		l[i] -= lm[i]
def l_sub(l, lm):
	b = list(l)
	for i in range(len(b)):
		b[i] -= lm[i]
	return b

def ip_floordiv(l, lm):
	for i in range(len(l)):
		l[i] //= lm[i]
def l_floordiv(l, lm):
	b = list(l)
	for i in range(len(b)):
		b[i] //= lm[i]
	return b

def ip_modulo(l, lm):
	for i in range(len(l)):
		l[i] %= lm[i]
def l_modulo(l, lm):
	b = list(l)
	for i in range(len(b)):
		b[i] %= lm[i]
	return b


class c_file: # python2-python3 polyglot file object
	def __init__(self, path, mode=None, encoding=None):
		self.path = path
		self.mode = mode
		self.encoding = encoding
		self.fp = None
		if self.mode == None: self.mode = 'r'
		if _IS_3 and self.encoding != None:
			self.fp = open(self.path, self.mode, encoding=self.encoding)
		else:
			self.fp = open(self.path, self.mode)
	def close(self):
		self.fp.close()
	def read(self, size=None):
		t = None
		if size == None:
			t = self.fp.read()
		else:
			t = self.fp.read(size)
		if _IS_3:
			return t
		else:
			return t.decode(self.encoding)
	def write(self, data):
		if _IS_3 or self.encoding == None:
			self.fp.write(data)
		else:
			self.fp.write(data.encode(self.encoding))
	def __enter__(self): # for use with 'with x as y'
		return self
	def __exit__(self, type, value, traceback):
		self.close()


