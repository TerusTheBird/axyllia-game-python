self.name = 'player_sprite'
r = 'res/sprites/human/'
self.sets.append( c_animation( r+'player_look_up.py' ))
self.sets.append( c_animation( r+'player_look_down.py' ))
self.sets.append( c_animation( r+'player_look_left.py' ))
self.sets.append( c_animation( r+'player_look_right.py' ))
self.sets.append( c_animation( r+'player_walk_up.py' ))
self.sets.append( c_animation( r+'player_walk_down.py' ))
self.sets.append( c_animation( r+'player_walk_left.py' ))
self.sets.append( c_animation( r+'player_walk_right.py' ))
self.set_cur = self.sets[1]

