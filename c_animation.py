# coding: utf-8
import sys, os, operator, pygame
from pygame.locals import *
sys.dont_write_bytecode = True

import c_pyglue
import c_constants as const

class c_animation: # generic animation
	def __init__(self, path):
		self.path = path
		self.name = None
		self.pos = [0,0]
		self.speed = 0
		self.frames = []
		
		self.frame = 0 # 960 fixed point
		
		# load pos, speed, frames, name
		with c_pyglue.c_file(path, 'r', 'utf-8') as f:
			exec(f.read()) # load animation file
	
	def load(self, frames):
		for f in frames:
			s = pygame.image.load(f)
			s.set_colorkey(const._transparent)
			self.frames.append(s)
	
	def reset(self):
		self.frame = 0
	
	def pump(self):
		self.frame = (self.frame + self.speed) % (len(self.frames) * const._sfp)
	
	def get_surface(self):
		return self.frames[ self.frame // const._sfp ]


class c_sprite:
	def __init__(self, path):
		self.sets = []
		self.set_cur = None
		self.set_last = None
		
		with c_pyglue.c_file(path, "r", 'utf-8') as f:
			exec(f.read()) # load sprite set file
	
	def change(self, name, dd=True):
		for anim in self.sets:
			if anim.name == name:
				old = self.set_cur
				self.set_cur = anim
				if (not (old is self.set_cur)) and dd:
					self.set_cur.reset()
				break
	
	def get_surface(self):
		if self.set_cur != None:
			return self.set_cur.get_surface()
	
	def get_animation(self):
		return self.set_cur
	
	def pump(self):
		if self.set_cur != None:
			self.set_cur.pump()





