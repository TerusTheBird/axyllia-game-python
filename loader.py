# coding: utf-8
import sys, os, operator, pygame
from pygame.locals import *
sys.dont_write_bytecode = True

import c_pyglue
import c_constants

import c_screen
import c_animation
import c_map
import c_game
import c_object

if __name__ == '__main__':
	a = c_game.c_game()
	a.map_load('res/maps/test1/map.py')
	
	test = c_object.c_object(a, 'res/npcs/player.py', [ (18-6)*16,(18-6)*16 ])
	test.press_left()
	a.objects.append( test )
	a.object_current = test
	
	a.objects.append(c_object.c_object(a, 'res/npcs/player.py', [ (19-6)*16,(18-6)*16 ]))
	
	a.run()
#

